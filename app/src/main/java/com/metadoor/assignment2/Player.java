package com.metadoor.assignment2;

/**
 * Created by metad00r on 28/11/15.
 */
public enum Player {
    HUMAN,
    COMPUTER,
    NONE
}