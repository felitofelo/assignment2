package com.metadoor.assignment2;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;


public class MainActivity extends Activity implements View.OnClickListener {

    private int count = 0;
    private final String X = "X";
    private final String O = "O";

    private Player turn;

    ArrayList<Button> buttons = new ArrayList<>();

    private Button btn0;
    private Button btn1;
    private Button btn2;
    private Button btn3;
    private Button btn4;
    private Button btn5;
    private Button btn6;
    private Button btn7;
    private Button btn8;
    private Button btnReset;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        turn = turn.HUMAN;

        btn0 = (Button) findViewById(R.id.pos0);
        btn1 = (Button) findViewById(R.id.pos1);
        btn2 = (Button) findViewById(R.id.pos2);
        btn3 = (Button) findViewById(R.id.pos3);
        btn4 = (Button) findViewById(R.id.pos4);
        btn5 = (Button) findViewById(R.id.pos5);
        btn6 = (Button) findViewById(R.id.pos6);
        btn7 = (Button) findViewById(R.id.pos7);
        btn8 = (Button) findViewById(R.id.pos8);
        btnReset = (Button) findViewById(R.id.resetBtn);

        btn0.setOnClickListener(this);
        btn1.setOnClickListener(this);
        btn2.setOnClickListener(this);
        btn3.setOnClickListener(this);
        btn4.setOnClickListener(this);
        btn5.setOnClickListener(this);
        btn6.setOnClickListener(this);
        btn7.setOnClickListener(this);
        btn8.setOnClickListener(this);
        btnReset.setOnClickListener(this);

        buttons.add(btn0);
        buttons.add(btn1);
        buttons.add(btn2);
        buttons.add(btn3);
        buttons.add(btn4);
        buttons.add(btn5);
        buttons.add(btn6);
        buttons.add(btn7);
        buttons.add(btn8);

        /*
            Added this save the button state upon rotating.
         */
        if (savedInstanceState != null) {
            btn0.setText(savedInstanceState.getString("1"));
            btn1.setText(savedInstanceState.getString("2"));
            btn2.setText(savedInstanceState.getString("3"));
            btn3.setText(savedInstanceState.getString("4"));
            btn4.setText(savedInstanceState.getString("5"));
            btn5.setText(savedInstanceState.getString("6"));
            btn6.setText(savedInstanceState.getString("7"));
            btn7.setText(savedInstanceState.getString("8"));
            btn8.setText(savedInstanceState.getString("9"));
        }
    }

    /*
        Added this save the button state upon rotating.
    */
    protected void onSaveInstanceState(Bundle outState)
    {
        outState.putString("1", btn0.getText().toString());
        outState.putString("2", btn1.getText().toString());
        outState.putString("3", btn2.getText().toString());
        outState.putString("4", btn3.getText().toString());
        outState.putString("5", btn4.getText().toString());
        outState.putString("6", btn5.getText().toString());
        outState.putString("7", btn6.getText().toString());
        outState.putString("8", btn7.getText().toString());
        outState.putString("9", btn8.getText().toString());

        super.onSaveInstanceState(outState);
    }
    
    @Override
    public void onClick(View v)
    {
        if(v.getId() == R.id.resetBtn)
        {
            resetGame();
        }
        else
        {
            if(turn == Player.HUMAN)
            {
                Button btnClicked = (Button) v;
                String btnText = btnClicked.getText().toString();

                if (btnText.length() < 1)
                {
                    btnClicked.setText(X);
                    count++;
                    boolean result = checkResult(btnClicked);

                    if (result) {
                        new AlertDialog.Builder(this)
                                .setTitle(this.getResources().getString(R.string.x_wins))
                                .setNeutralButton(android.R.string.yes, null)
                                .setIcon(android.R.drawable.ic_dialog_alert)
                                .show();
                        turn = Player.NONE;
                    }
                    else
                    {
                        if(count == 9)
                        {
                            new AlertDialog.Builder(this)
                                    .setTitle(this.getResources().getString(R.string.draw))
                                    .setNeutralButton(android.R.string.yes, null)
                                    .setIcon(android.R.drawable.ic_dialog_alert)
                                    .show();
                        }
                        else
                        {
                            turn = Player.COMPUTER;
                            computerMove();
                        }
                    }
                }
            }
        }
    }

    /**
     * New game
     */
    private void resetGame()
    {
        for(Button button : buttons) button.setText("");
        count = 0;
        turn = Player.HUMAN;
    }

    /**
     *
     * @return
     */
    private ArrayList<Button> saveCurrentState()
    {
        ArrayList<Button> buttons = new ArrayList<>();

        for(Button button : this.buttons) {
            buttons.add(button);
        }

        return buttons;
    }

    /**
     * Intelligent computer move
     */
    public void computerMove()
    {
        Button btn = smartCheck();
        btn.setText(O);
        count++;
        boolean result = checkResult(btn);

        if(result)
        {
            new AlertDialog.Builder(this)
                    .setTitle(this.getResources().getString(R.string.o_wins))
                    .setNeutralButton(android.R.string.yes, null)
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .show();
        }
        else
        {
            if(count == 9)
            {
                new AlertDialog.Builder(this)
                        .setTitle(this.getResources().getString(R.string.draw))
                        .setNeutralButton(android.R.string.yes, null)
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .show();
                turn = Player.NONE;
            }
            else turn = Player.HUMAN;
        }
}

    /**
     *
     * @return Button
     */
    private Button smartCheck()
    {
        Random rand = new Random();
        int r = rand.nextInt(7);
        Button smartPos = buttons.get(r);

        turn = Player.HUMAN;
        for (Button button : buttons)
            if (button.getText() == "")
                if (checkResult(button)) smartPos = button;

        turn = Player.COMPUTER;
        for (Button button : buttons)
            if (button.getText() == "")
                if (checkResult(button)) smartPos = button;

        for(Button button : buttons)
            if(button.getId() == R.id.pos4)
                if(button.getText() == "") smartPos = button;

        if(smartPos.getText() != "") smartCheck();
        return smartPos;
    }

    /**
     *
     * @param b
     * @return boolean
     */
    private boolean checkResult(Button b)
    {
        boolean won = false;
        HashMap<Integer, Integer> checkPos = new HashMap<>();

        int pos = b.getId();
        switch (pos)
        {
            case R.id.pos0:
                checkPos.put(R.id.pos1, R.id.pos2);
                checkPos.put(R.id.pos3, R.id.pos6);
                checkPos.put(R.id.pos4, R.id.pos8);
                won = checkPos(checkPos);
                break;
            case R.id.pos1:
                checkPos.put(R.id.pos0, R.id.pos2);
                checkPos.put(R.id.pos4, R.id.pos7);
                won = checkPos(checkPos);
                break;
            case R.id.pos2:
                checkPos.put(R.id.pos0, R.id.pos1);
                checkPos.put(R.id.pos4, R.id.pos6);
                checkPos.put(R.id.pos5, R.id.pos8);
                won = checkPos(checkPos);
                break;
            case R.id.pos3:
                checkPos.put(R.id.pos0, R.id.pos6);
                checkPos.put(R.id.pos4, R.id.pos5);
                won = checkPos(checkPos);
                break;
            case R.id.pos4:
                checkPos.put(R.id.pos0, R.id.pos8);
                checkPos.put(R.id.pos1, R.id.pos7);
                checkPos.put(R.id.pos2, R.id.pos6);
                checkPos.put(R.id.pos3, R.id.pos5);
                won = checkPos(checkPos);
                break;
            case R.id.pos5:
                checkPos.put(R.id.pos2, R.id.pos8);
                checkPos.put(R.id.pos3, R.id.pos4);
                won = checkPos(checkPos);
                break;
            case R.id.pos6:
                checkPos.put(R.id.pos0, R.id.pos3);
                checkPos.put(R.id.pos2, R.id.pos4);
                checkPos.put(R.id.pos7, R.id.pos8);
                won = checkPos(checkPos);
                break;
            case R.id.pos7:
                checkPos.put(R.id.pos1, R.id.pos4);
                checkPos.put(R.id.pos6, R.id.pos8);
                won = checkPos(checkPos);
                break;
            case R.id.pos8:
                checkPos.put(R.id.pos0, R.id.pos4);
                checkPos.put(R.id.pos2, R.id.pos5);
                checkPos.put(R.id.pos6, R.id.pos7);
                won = checkPos(checkPos);
                break;
        }

        return won;
    }

    /**
     *
     * @param checkPos
     * @return
     */
    private boolean checkPos(HashMap<Integer, Integer> checkPos)
    {
        String symbol = (turn == Player.HUMAN) ? X : O;

        for(Map.Entry<Integer, Integer> posSet : checkPos.entrySet())
        {
            Button result0Btn = (Button) findViewById(posSet.getKey());
            Button result1Btn = (Button) findViewById(posSet.getValue());
            String result0 = result0Btn.getText().toString();
            String result1 = result1Btn.getText().toString();

            if(result0 == symbol && result1 == symbol)
                return true;
        }
        return false;
    }
}
